Philips Hue smart lights can be controlled by OpenRGB.  These bulbs must be set up and paired to the Hue Bridge first using the official Hue app, but after they are connected to your bridge you can add your Hue bridge to the OpenRGB configuration file to control them.

An example Philips Hue configuration entry:

```
    "PhilipsHueDevices": {
        "bridges": [
            {
                "clientkey": "1BC41F53EAFD7E5DDD6823440D1D33AF",
                "entertainment": true,
                "ip": "192.168.3.176",
                "mac": "EC:B5:FA:15:3D:CD",
                "username": "IJs5ZwN3Px-fzjVkZROpfmU7hKmy2GO0vtN6xhRV"
            }
        ]
    },
```

* `ip` - IP address of Hue bridge.  You can find this in your router's administration page.
* `mac` - MAC address of Hue bridge.  You can find this in your router's administration page.
* `entertainment` - Boolean, values are `true` or `false`.  When `true`, uses Entertainment Mode to provide fast update rates.  Requires a V2 bridge or newer and an Entertainment Group created in the app.
* `clientkey` - Omit this field entirely when creating the file, it will be filled in automatically upon pairing with the bridge.
* `username` - Omit this field entirely when creating the file, it will be filled in automatically upon pairing with the bridge.

After adding the configuration, when opening OpenRGB for the first time with the configuration added, you must press the pair button on the bridge.  This will register OpenRGB with the bridge and store the clientkey and username in the configuration.