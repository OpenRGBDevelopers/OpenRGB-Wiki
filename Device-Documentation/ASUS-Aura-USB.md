# Asus Aura USB

Some of the documentation provided here comes from @inlart's blog: <https://blog.inlart.com/post/openrgb-asus-x570/>

Beginning with the AMD X470 chipset generation, Asus started using USB-based Aura controllers.  On the X470 generation boards, an Aura SMBus controller drives the on-board lighting and the 12V RGB headers while an Aura USB controller drives the addressable headers.  For the X570 generation, ASUS switched to a single Aura USB controller that controls on-board lighting, 12V RGB non-addressable headers, and addressable headers from a single location.  The protocols of these two types of Aura USB controllers differ but have some overlap.  This page documents the X570-generation Aura USB controller while the [ASUS Aura Addressable Header](ASUS-Aura-Addressable-Header) page documents the X470-generation addressable header controller that also powers the ROG Aura Terminal.

The Aura USB controller enumerates at 0B05:18F3, 0B05:1939 and 0B05:19AF.  It uses HID read and write operations for control.  Messages are 65 bytes long and zero-filled.

## **Request Firmware String**

| Byte index | Value |
| ---------- | ----- |
| 0x00       | 0xEC  |
| 0x01       | 0x82  |

## Firmware String Response

| Byte index | Value                                   |
| ---------- | --------------------------------------- |
| 0x00       | 0xEC                                    |
| 0x01       | 0x02                                    |
| 0x02-0x12  | Firmware string (ex. "AUTA0-S072-0101") |

## **Request Configuration Table**

| Byte index | Value |
| ---------- | ----- |
| 0x00       | 0xEC  |
| 0x01       | 0xB0  |

## Configuration Table Response

| Byte index | Value                               |
| ---------- | ----------------------------------- |
| 0x00       | 0xEC                                |
| 0x01       | 0x30                                |
| 0x03+      | Configuration Table Data (60 bytes) |

## **Set Effect Mode**

| Byte Index | Description                                                                                                  |
| ---------- | ------------------------------------------------------------------------------------------------------------ |
| 0x00       | 0xEC                                                                                                         |
| 0x01       | 0x35                                                                                                         |
| 0x02       | Channel type (0x00 for fixed, 0x01 for addressable, or 0x00 if the board only provides addressable channels) |
| 0x03       | 0x00                                                                                                         |
| 0x04       | 0x00: apply as normal effect, 0x01: apply as shutdown effect                                                 |
| 0x05       | Mode/Effect ID                                                                                               |
| 0x06+      | Unknown bytes (possibly brightness or animation speed), setting everything to 0x00 works fine                |

Shutdown effect only works with onboard LEDs.

## **Set Colors**

| Byte Index | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| 0x00       | 0xEC                                                         |
| 0x01       | 0x36                                                         |
| 0x02       | LED mask (most significant byte)                             |
| 0x03       | LED mask (least significant byte)                            |
| 0x04       | 0x00: apply as normal effect, 0x01: apply as shutdown effect |
| 0x05+      | RGB color data                                               |

For fixed channel, colors are set for each individual LED.  For each addressable channel, only a single color can be specified.
Bits in LED mask are set according to which LEDs you're changing.  (`led_mask = ((1 << led_count) - 1) << led_start`)

Shutdown effect only works with onboard LEDs.

## **Set Direct Mode Colors**

| Byte Index | Description                           |
| ---------- | ------------------------------------- |
| 0x00       | 0xEC                                  |
| 0x01       | 0x40                                  |
| 0x02       | `(apply ? 0x80 : 0x00) \| channel_id` |
| 0x03       | Start LED                             |
| 0x04       | LED count                             |
| 0x05+      | RGB color data                        |

`channel_id` is 0x00 - 0x03 for addressable channels, and 0x04 for fixed channel

## **Commit**

Used to save the current effect, the shutdown effect and their colors (direct mode is saved as a rainbow effect) so they can be automatically restored after a power cycle.

| Byte Index | Value |
| ---------- | ----- |
| 0x00       | 0xEC  |
| 0x01       | 0x3F  |
| 0x02       | 0x55  |

## **Lighting Off/On**

Possibly related to the LED lighting options found in the BIOS.  Committing after this command doesn't save it even though Armoury Crate is able to change the BIOS options somehow.

| Byte Index | Description                                                                                          |
| ---------- | ---------------------------------------------------------------------------------------------------- |
| 0x00       | 0xEC                                                                                                 |
| 0x01       | 0x38                                                                                                 |
| 0x02       | 0x00: when system is in sleep, hibernate or soft off states?, 0x01: when system is in working state? |
| 0x03       | 0x00: off, 0x01: on                                                                                  |