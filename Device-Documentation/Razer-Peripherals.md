# Razer Peripherals

Razer Huntsman Elite keymap:

6 Rows x 22 Columns

| LED Row | LED Column | Key               |
| ------- | ---------- | ----------------- |
| 0       | 0          |                   |
| 0       | 1          | Key: Escape       |
| 0       | 2          |                   |
| 0       | 3          | Key: F1           |
| 0       | 4          | Key: F2           |
| 0       | 5          | Key: F3           |
| 0       | 6          | Key: F4           |
| 0       | 7          | Key: F5           |
| 0       | 8          | Key: F6           |
| 0       | 9          | Key: F7           |
| 0       | 10         | Key: F8           |
| 0       | 11         | Key: F9           |
| 0       | 12         | Key: F10          |
| 0       | 13         | Key: F11          |
| 0       | 14         | Key: F12          |
| 0       | 15         | Key: Print Screen |
| 0       | 16         | Key: Scroll Lock  |
| 0       | 17         | Key: Pause/Break  |
| 0       | 18         | Media Previous    |
| 0       | 19         | Media Play/Pause  |
| 0       | 20         | Media Next        |
| 0       | 21         | Media Volume/Mute |
| 1       | 0          |                   |
| 1       | 1          | Key: `            |
| 1       | 2          | Key: 1            |
| 1       | 3          | Key: 2            |
| 1       | 4          | Key: 3            |
| 1       | 5          | Key: 4            |
| 1       | 6          | Key: 5            |
| 1       | 7          | Key: 6            |
| 1       | 8          | Key: 7            |
| 1       | 9          | Key: 8            |
| 1       | 10         | Key: 9            |
| 1       | 11         | Key: 0            |
| 1       | 12         | Key: -            |
| 1       | 13         | Key: =            |
| 1       | 14         | Key: Backspace    |
| 1       | 15         | Key: Insert       |
| 1       | 16         | Key: Home         |
| 1       | 17         | Key: Page Up      |
| 1       | 18         | Key: Num Lock     |
| 1       | 19         | Key: Num Pad /    |
| 1       | 20         | Key: Num Pad *    |
| 1       | 21         | Key: Num Pad -    |
| 2       | 0          |                   |
| 2       | 1          | Key: Tab          |
| 2       | 2          | Key: Q            |
| 2       | 3          | Key: W            |
| 2       | 4          | Key: E            |
| 2       | 5          | Key: R            |
| 2       | 6          | Key: T            |
| 2       | 7          | Key: Y            |
| 2       | 8          | Key: U            |
| 2       | 9          | Key: I            |
| 2       | 10         | Key: O            |
| 2       | 11         | Key: P            |
| 2       | 12         | Key: [            |
| 2       | 13         | Key: ]            |
| 2       | 14         | Key: \\           |
| 2       | 15         | Key: Delete       |
| 2       | 16         | Key: End          |
| 2       | 17         | Key: Page Down    |
| 2       | 18         | Key: Num Pad 7    |
| 2       | 19         | Key: Num Pad 8    |
| 2       | 20         | Key: Num Pad 9    |
| 2       | 21         | Key: Num Pad +    |
| 3       | 0          |                   |
| 3       | 1          | Key: Caps Lock    |
| 3       | 2          | Key: A            |
| 3       | 3          | Key: S            |
| 3       | 4          | Key: D            |
| 3       | 5          | Key: F            |
| 3       | 6          | Key: G            |
| 3       | 7          | Key: H            |
| 3       | 8          | Key: J            |
| 3       | 9          | Key: K            |
| 3       | 10         | Key: L            |
| 3       | 11         | Key: ;            |
| 3       | 12         | Key: '            |
| 3       | 13         |                   |
| 3       | 14         | Key: Enter        |
| 3       | 15         |                   |
| 3       | 16         |                   |
| 3       | 17         |                   |
| 3       | 18         | Key: Num Pad 4    |
| 3       | 19         | Key: Num Pad 5    |
| 3       | 20         | Key: Num Pad 6    |
| 3       | 21         |                   |
| 4       | 0          |                   |
| 4       | 1          | Key: Left Shift   |
| 4       | 2          |                   |
| 4       | 3          | Key: Z            |
| 4       | 4          | Key: X            |
| 4       | 5          | Key: C            |
| 4       | 6          | Key: V            |
| 4       | 7          | Key: B            |
| 4       | 8          | Key: N            |
| 4       | 9          | Key: M            |
| 4       | 10         | Key: ,            |
| 4       | 11         | Key: .            |
| 4       | 12         | Key: /            |
| 4       | 13         |                   |
| 4       | 14         | Key: Right Shift  |
| 4       | 15         |                   |
| 4       | 16         | Key: Up Arrow     |
| 4       | 17         |                   |
| 4       | 18         | Key: Num Pad 1    |
| 4       | 19         | Key: Num Pad 2    |
| 4       | 20         | Key: Num Pad 3    |
| 4       | 21         | Key: Num Pad Enter|
| 5       | 0          |                   |
| 5       | 1          | Key: Left Control |
| 5       | 2          | Key: Windows      |
| 5       | 3          | Key: Left Alt     |
| 5       | 4          |                   |
| 5       | 5          |                   |
| 5       | 6          |                   |
| 5       | 7          | Key: Space        |
| 5       | 8          |                   |
| 5       | 9          |                   |
| 5       | 10         |                   |
| 5       | 11         | Key: Right Alt    |
| 5       | 12         | Key: Function     |
| 5       | 13         | Key: Context      |
| 5       | 14         | Key: Right Control|
| 5       | 15         | Key: Left Arrow   |
| 5       | 16         | Key: Down Arrow   |
| 5       | 17         | Key: Right Arrow  |
| 5       | 18         |                   |
| 5       | 19         | Key: Num Pad 0    |
| 5       | 20         | Key: Num Pad .    |
| 5       | 21         |                   |

Razer Blade Pro (2017):

6 Rows x 25 Columns

| LED Row | LED Column | Key                   |
| ------- | ---------- | --------------------- |
| 0       | 0          |                       |
| 0       | 1          |                       |
| 0       | 2          | Key: Escape           |
| 0       | 3          | Key: F1               |
| 0       | 4          | Key: F2               |
| 0       | 5          | Key: F3               |
| 0       | 6          | Key: F4               |
| 0       | 7          | Key: F5               |
| 0       | 8          | Key: F6               |
| 0       | 9          | Key: F7               |
| 0       | 10         | Key: F8               |
| 0       | 11         | Key: F9               |
| 0       | 12         | Key: F10              |
| 0       | 13         | Key: F11              |
| 0       | 14         | Key: F12              |
| 0       | 15         | Key: Insert           |
| 0       | 16         |                       |
| 0       | 17         | Key: Delete           |
| 0       | 18         |                       |
| 0       | 19         | Media Previous        |
| 0       | 20         | Media Next            |
| 0       | 21         | Volume Wheel          |
| 0       | 22         |                       |
| 0       | 23         | Media Play/Pause      |
| 0       | 24         | Media Mute            |
| 1       | 0          |                       |
| 1       | 1          |                       |
| 1       | 2          | Key: `                |
| 1       | 3          | Key: 1                |
| 1       | 4          | Key: 2                |
| 1       | 5          | Key: 3                |
| 1       | 6          | Key: 4                |
| 1       | 7          | Key: 5                |
| 1       | 8          | Key: 6                |
| 1       | 9          | Key: 7                |
| 1       | 10         | Key: 8                |
| 1       | 11         | Key: 9                |
| 1       | 12         | Key: 0                |
| 1       | 13         | Key: -                |
| 1       | 14         | Key: =                |
| 1       | 15         | Key: Backspace        |
| 1       | 16         |                       |
| 1       | 17         |                       |
| 1       | 18         |                       |
| 1       | 19         | Trackpad Row 0, Col 0 |
| 1       | 20         | Trackpad Row 0, Col 1 |
| 1       | 21         | Trackpad Row 0, Col 2 |
| 1       | 22         | Trackpad Row 0, Col 3 |
| 1       | 23         | Trackpad Row 0, Col 4 |
| 1       | 24         | Trackpad Row 0, Col 5 |
| 2       | 0          |                       |
| 2       | 1          |                       |
| 2       | 2          | Key: Tab              |
| 2       | 3          |                       |
| 2       | 4          | Key: Q                |
| 2       | 5          | Key: W                |
| 2       | 6          | Key: E                |
| 2       | 7          | Key: R                |
| 2       | 8          | Key: T                |
| 2       | 9          | Key: Y                |
| 2       | 10         | Key: U                |
| 2       | 11         | Key: I                |
| 2       | 12         | Key: O                |
| 2       | 13         | Key: P                |
| 2       | 14         | Key: [                |
| 2       | 15         | Key: ]                |
| 2       | 16         |                       |
| 2       | 17         | Key: \\               |
| 2       | 18         |                       |
| 2       | 19         | Trackpad Row 1, Col 0 |
| 2       | 20         |                       |
| 2       | 21         |                       |
| 2       | 22         |                       |
| 2       | 23         |                       |
| 2       | 24         | Trackpad Row 1, Col 5 |
| 3       | 0          |                       |
| 3       | 1          | Key: Caps Lock        |
| 3       | 2          |                       |
| 3       | 3          |                       |
| 3       | 4          | Key: A                |
| 3       | 5          | Key: S                |
| 3       | 6          | Key: D                |
| 3       | 7          | Key: F                |
| 3       | 8          | Key: G                |
| 3       | 9          | Key: H                |
| 3       | 10         | Key: J                |
| 3       | 11         | Key: K                |
| 3       | 12         | Key: L                |
| 3       | 13         | Key: ;                |
| 3       | 14         | Key: '                |
| 3       | 15         |                       |
| 3       | 16         |                       |
| 3       | 17         |                       |
| 3       | 18         | Key: Enter            |
| 3       | 19         | Trackpad Row 2, Col 0 |
| 3       | 20         |                       |
| 3       | 21         |                       |
| 3       | 22         |                       |
| 3       | 23         |                       |
| 3       | 24         | Trackpad Row 2, Col 5 |
| 4       | 0          | Key: Left Shift       |
| 4       | 1          |                       |
| 4       | 2          |                       |
| 4       | 3          |                       |
| 4       | 4          | Key: Z                |
| 4       | 5          | Key: X                |
| 4       | 6          | Key: C                |
| 4       | 7          | Key: V                |
| 4       | 8          | Key: B                |
| 4       | 9          | Key: N                |
| 4       | 10         | Key: M                |
| 4       | 11         | Key: ,                |
| 4       | 12         | Key: .                |
| 4       | 13         | Key: /                |
| 4       | 14         | Key: Up Arrow         |
| 4       | 15         |                       |
| 4       | 16         |                       |
| 4       | 17         |                       |
| 4       | 18         | Key: Right Shift      |
| 4       | 19         | Trackpad Row 3, Col 0 |
| 4       | 20         |                       |
| 4       | 21         |                       |
| 4       | 22         |                       |
| 4       | 23         |                       |
| 4       | 24         | Trackpad Row 3, Col 5 |
| 5       | 0          | Key: Left Control     |
| 5       | 1          |                       |
| 5       | 2          | Key: Left Function    |
| 5       | 3          | Key: Windows          |
| 5       | 4          |                       |
| 5       | 5          | Key: Left Alt         |
| 5       | 6          |                       |
| 5       | 7          | Key: Space            |
| 5       | 8          |                       |
| 5       | 9          |                       |
| 5       | 10         | Key: Right Alt        |
| 5       | 11         |                       |
| 5       | 12         | Key: Right Control    |
| 5       | 13         | Key: Left Arrow       |
| 5       | 14         | Key: Down Arrow       |
| 5       | 15         | Key: Right Arrow      |
| 5       | 16         | Key: Right Function   |
| 5       | 17         |                       |
| 5       | 18         |                       |
| 5       | 19         | Trackpad Row 4, Col 0 |
| 5       | 20         | Trackpad Row 4, Col 1 |
| 5       | 21         | Trackpad Row 4, Col 2 |
| 5       | 22         | Trackpad Row 4, Col 3 |
| 5       | 23         | Trackpad Row 4, Col 4 |
| 5       | 24         | Trackpad Row 4, Col 5 |

Razer Blade Stealth:

| LED Row | LED Column | Key                      |
| ------- | ---------- | ------------------------ |
| 0       | 0          |                          |
| 0       | 1          | Key: Escape              |
| 0       | 2          | Key: F1                  |
| 0       | 3          | Key: F2                  |
| 0       | 4          | Key: F3                  |
| 0       | 5          | Key: F4                  |
| 0       | 6          | Key: F5                  |
| 0       | 7          | Key: F6                  |
| 0       | 8          | Key: F7                  |
| 0       | 9          | Key: F8                  |
| 0       | 10         | Key: F9                  |
| 0       | 11         | Key: F10                 |
| 0       | 12         | Key: F11                 |
| 0       | 13         | Key: F12                 |
| 0       | 14         | Key: Insert              |
| 0       | 15         | Key: Delete              |
| 1       | 0          |                          |
| 1       | 1          | Key: `                   |
| 1       | 2          | Key: 1                   |
| 1       | 3          | Key: 2                   |
| 1       | 4          | Key: 3                   |
| 1       | 5          | Key: 4                   |
| 1       | 6          | Key: 5                   |
| 1       | 7          | Key: 6                   |
| 1       | 8          | Key: 7                   |
| 1       | 9          | Key: 8                   |
| 1       | 10         | Key: 9                   |
| 1       | 11         | Key: 0                   |
| 1       | 12         | Key: -                   |
| 1       | 13         | Key: =                   |
| 1       | 14         | Key: Backspace (Left)    |
| 1       | 15         | Key: Backspace (Right)   |
| 2       | 0          | Key: Tab                 |
| 2       | 1          |                          |
| 2       | 2          | Key: Q                   |
| 2       | 3          | Key: W                   |
| 2       | 4          | Key: E                   |
| 2       | 5          | Key: R                   |
| 2       | 6          | Key: T                   |
| 2       | 7          | Key: Y                   |
| 2       | 8          | Key: U                   |
| 2       | 9          | Key: I                   |
| 2       | 10         | Key: O                   |
| 2       | 11         | Key: P                   |
| 2       | 12         | Key: [                   |
| 2       | 13         | Key: ]                   |
| 2       | 14         | Key: \ (Left)            |
| 2       | 15         | Key: \ (Right)           |
| 3       | 0          | Key: Caps Lock           |
| 3       | 1          |                          |
| 3       | 2          | Key: A                   |
| 3       | 3          | Key: S                   |
| 3       | 4          | Key: D                   |
| 3       | 5          | Key: F                   |
| 3       | 6          | Key: G                   |
| 3       | 7          | Key: H                   |
| 3       | 8          | Key: J                   |
| 3       | 9          | Key: K                   |
| 3       | 10         | Key: L                   |
| 3       | 11         | Key: ;                   |
| 3       | 12         | Key: '                   |
| 3       | 13         |                          |
| 3       | 14         | Key: Enter (Left)        |
| 3       | 15         | Key: Enter (Right)       |
| 4       | 0          | Key: Left Shift          |
| 4       | 1          |                          |
| 4       | 2          | Key: Z                   |
| 4       | 3          | Key: X                   |
| 4       | 4          | Key: C                   |
| 4       | 5          | Key: V                   |
| 4       | 6          | Key: B                   |
| 4       | 7          | Key: N                   |
| 4       | 8          | Key: M                   |
| 4       | 9          | Key: ,                   |
| 4       | 10         | Key: .                   |
| 4       | 11         | Key: /                   |
| 4       | 12         | Key: Right Shift (Left)  |
| 4       | 13         | Key: Right Shift (Mid)   |
| 4       | 14         | Key: Right Shift (Right) |
| 4       | 15         |                          |
| 5       | 0          | Key: Left Control        |
| 5       | 1          | Key: Left Function       |
| 5       | 2          | Key: Left Windows        |
| 5       | 3          | Key: Left Alt            |
| 5       | 4          |                          |
| 5       | 5          | Key: Space (Left)        |
| 5       | 6          | Key: Space (Mid)         |
| 5       | 7          |                          |
| 5       | 8          | Key: Space (Right)       |
| 5       | 9          | Key: Right Alt           |
| 5       | 10         | Key: Right Function      |
| 5       | 11         | Key: Right Control       |
| 5       | 12         | Key: Left Arrow          |
| 5       | 13         | Key: Up Arrow            |
| 5       | 14         | Key: Right Arrow         |
| 5       | 15         | Key: Down Arrow          |
