# OpenRGB Windows Setup and Usage

This page describes how to set up and use OpenRGB on your Windows PC.

## **Download OpenRGB**

You can download the latest version of OpenRGB from the [Releases Page](https://gitlab.com/CalcProgrammer1/OpenRGB/-/releases)

The builds are provided for both 32- and 64-bit Windows.  They are provided as .7z archives.  Use [7-zip](https://www.7-zip.org/) to extract them.  You should end up with a folder containing the OpenRGB files.

![image](../uploads/d5dbd97f813532669a5c0a99aff12980/image.png)

## **Disable other RGB applications**

Before you open OpenRGB, make sure all other RGB applications are closed.  This includes ASUS Aura, RGB Fusion, iCue, Mystic Light, Razer Synapse, Logitech G Hub, Asrock Polychrome, and any other RGB app you may have active.  Note that some of these run services that you can stop in Task Manager.  For instance, LightingService for ASUS Aura.

![image](../uploads/18c383a7b1b6fb3becc1ce7495b31f64/image.png)

## **First Run**

When opening OpenRGB.exe for the first time, you must run it as Administrator for it to detect certain devices.  This installs a driver called inpout32 (inpoutx64 for 64-bit) that allows OpenRGB to access certain motherboards and RGB RAM.

![image](../uploads/c0cc51040d5f1026031728b67fe31ffb/image.png)

## **It didn't detect my device!  What now?**

Please [uninstall WinUSB](https://gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/Frequently-Asked-Questions#i-installed-the-winusb-driver-for-a-device-and-i-wish-to-uninstall-it) if it is installed.

if you never used zidag then you shouldn't have to worry about this

## **It STILL didn't detect my device!**

It's possible your device is in a weird state.  If you've done the above things and are not seeing your device show up when opening OpenRGB, you can try fully powering off your computer.  For best results, you should switch off the power supply or unplug the computer from the wall, just to make sure it is completely off.  Some motherboard RGB controllers remain powered in standby mode (when the PC is "off") and I've had my Aura controller need resetting from a full disconnect.  Plug it back in, switch it back on, and boot up.  Don't open any other RGB app, just open OpenRGB right away.  Hopefully that will get it detected and working.

