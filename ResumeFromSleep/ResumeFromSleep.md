# Making OpenRGB resume after sleeping (Or undoing it)

It is no longer required to configure Task scheduler (Windows) or systemd (Linux) manually to handle sleeping.
Now, this can be accomplished in the OpenRGB Settings page.

For users with RGB RAM or similar devices that stay powered when the computer is asleep, create a profile that turns it off and set it up with the `Set Profile on Suspend` option.
Also set up a `Set Profile on Resume` to restore your RAM modes when waking up.

For users with devices that don't have a flash to remember modes through a power cycle, load the profile back on with the `Set Profile on Resume` option.

## Removing the old way

If you set up Task scheduler or systemd previously using these instructions, use the following to remove that integration so you can just use the OpenRGB Settings page instead.

### Removing from linux

1. Open a terminal and ``cd /usr/lib/systemd/system-sleep``

2. type in ``sudo rm OpenRGBsleep.sh`` and hit enter (This *will* delete the file)

You should be good to go now

### Removing from Windows

1. Go to task scheduler

2. in the active tasks page search for whatever you named your task (search by clicking any of the tasks and then start typing)

3. Once your task has been located you can double click and delete on the far right side actions menubar (near the bottom of the bar)

The task should be gone now and it won't run again
